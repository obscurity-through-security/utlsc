uTLS C binding prototype

based on https://github.com/refraction-networking/utls (BSD)
see example/ for example
see binding/binding.go or run make and see bin/libultsc.h to see docs

build on Windows

prepare all necessary tool and run make
same as other cgo program, gcc is required
tested with TDM-GCC 10 and GNU make 3.81 on Windows 10 amd64
example is not working due to different socket and thread API
