﻿using System.Runtime.InteropServices;

namespace Nutls
{
    public class C
    {
        const string dll = "utlsc";

        [DllImport(dll)]
        public static extern int utls_new();

        [DllImport(dll)]
        [return: MarshalAs(UnmanagedType.LPStr)]
        public static extern string utls_error(int h);

        [DllImport(dll)]
        public static extern int utls_set_ca(int h, [In, Out] byte[] pCert, uint lSizeOfCert);

        [DllImport(dll)]
        public static extern int utls_set_server_name(int h, [MarshalAs(UnmanagedType.LPStr)] string szName);

        [DllImport(dll)]
        public static extern int utls_skip_check(int h, int bCheck);

        [DllImport(dll)]
        public static extern int utls_set_alpn(int h, [MarshalAs(UnmanagedType.LPStr)] string szAlpn);

        [DllImport(dll)]
        public static extern int utls_set_alps(int h, [MarshalAs(UnmanagedType.LPStr)] string szApp, [In, Out] byte[] pConfig, uint lSizeOfConfig);

        [DllImport(dll)]
        public static extern int utls_read_alps(int h, [In, Out] byte[] pData, uint lSizeOfData);

        [DllImport(dll)]
        public static extern int utls_read_server_name(int h, [In, Out] byte[] pData, uint lSizeOfData);

        [DllImport(dll)]
        public static extern int utls_set_fingerprint(int h, int lFingerprintID);

        [DllImport(dll)]
        public static extern int utls_start(int h);

        [DllImport(dll)]
        public static extern int utls_write(int h, [In, Out] byte[] pData, uint lSizeOfData);

        [DllImport(dll)]
        public static extern int utls_read(int h, [In, Out] byte[] pData, uint lSizeOfData);

        [DllImport(dll)]
        public static extern int utls_read_tls(int h, [In, Out] byte[] pData, uint lSizeOfData);

        [DllImport(dll)]
        public static extern int utls_write_tls(int h, [In, Out] byte[] pData, uint lSizeOfData);

        [DllImport(dll)]
        public static extern int utls_close(int h);

        [DllImport(dll)]
        public static extern int utls_clone_fingerprint(int h, [In, Out] byte[] pData, uint lSizeOfData, int bShuffle, int bSloppy);

        [DllImport(dll)]
        public static extern int utls_random_fingerprint([In, Out] byte[] pData, uint lSizeOfData);

        [DllImport(dll)]
        public static extern void utls_enable_weak_cipher();

    }
}
