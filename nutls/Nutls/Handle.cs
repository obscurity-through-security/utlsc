using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Nutls
{
    [System.Serializable]
    public class UTLSException : System.Exception
    {
        public UTLSException(string message) : base(message) { }
    }

    class Handle
    {
        int fd;

        public Handle()
        {
            fd = C.utls_new();
        }

        private static int chk(int ret)
        {
            if (ret < 0)
            {
                string err = C.utls_error(0);
                throw new UTLSException(err);
            }
            return ret;
        }

        private static int chkbuf(int ret)
        {
            if (ret >= 0)
            {
                return 0;
            }
            if (ret != -1)
            {
                return -ret;
            }
            string err = C.utls_error(0);
            if (!string.IsNullOrWhiteSpace(err))
            {
                throw new UTLSException(err);
            }
            return -ret;
        }

        private int bin(bool b)
        {
            return b ? 1 : 0;
        }

        public void SetCA(X509CertificateCollection ca)
        {
            List<byte> b = new List<byte>();
            foreach (var cert in ca)
            {
                b.AddRange(cert.GetRawCertData());
            }
            byte[] arr = b.ToArray();
            chk(C.utls_set_ca(fd, arr, (uint)arr.Length));
        }
        public void SetServerName(string sni)
        {
            chk(C.utls_set_server_name(fd, sni));
        }
        public void SkipCheck(bool y)
        {
            chk(C.utls_skip_check(fd, bin(y)));
        }
        public void SetALPN(string alpn)
        {
            chk(C.utls_set_alpn(fd, alpn));
        }
        public void SetALPS(string app, byte[] conf)
        {
            chk(C.utls_set_alps(fd, app, conf, (uint)conf.Length));
        }
        public byte[] ReadALPS()
        {
            int size = chkbuf(C.utls_read_alps(fd, null, 0));
            byte[] buf = new byte[size];
            int l = chk(C.utls_read_alps(fd, buf, (uint)buf.Length));
            if (l > size)
            {
                throw new UTLSException("read too much");
            }
            if (l == size)
            {
                return buf;
            }
            byte[] r = new byte[l];
            Array.Copy(buf, 0, r, 0, l);
            return r;
        }
        public void Start()
        {
            chk(C.utls_start(fd));
        }

        private byte[] slice(byte[] b, int off, int n)
        {
            if (off == 0)
            {
                return b;
            }
            byte[] t = new byte[n - off];
            Array.Copy(b, off, t, 0, n);
            return t;
        }
        public int Write(byte[] b, int off, int n)
        {
            return chk(C.utls_write(fd, slice(b, off, n), (uint)n));
        }
        public int WriteTLS(byte[] b, int off, int n)
        {
            return chk(C.utls_write_tls(fd, slice(b, off, n), (uint)n));
        }

        private int readF(Func<int, byte[], uint, int> fn, byte[] b, int off, int n)
        {
            byte[] t = b;
            if (off != 0)
            {
                t = new byte[n];
            }
            int r = chk(fn(fd, t, (uint)n));
            if (off != 0)
            {
                Array.Copy(t, 0, b, off, r);
            }
            return r;
        }

        public int Read(byte[] b, int off, int n)
        {
            return readF(C.utls_read, b, off, n);
        }
        public int ReadTLS(byte[] b, int off, int n)
        {
            return readF(C.utls_read_tls, b, off, n);
        }
        public void Close()
        {
            chk(C.utls_close(fd));
        }
        public void CloneFingerprint(byte[] fingerprint, bool shuffle = false, bool sloppy = false)
        {
            chk(C.utls_clone_fingerprint(fd, fingerprint, (uint)fingerprint.Length, bin(shuffle), bin(sloppy)));
        }

        public void SetFingerprint(FingerprintID id)
        {
            chk(C.utls_set_fingerprint(fd, (int)id));
        }

        static byte[] RandomFingerprint()
        {
            int size = chkbuf(C.utls_random_fingerprint(null, 0));
            byte[] buf = new byte[size];
            int l = chk(C.utls_random_fingerprint(buf, (uint)buf.Length));
            if (l > size)
            {
                throw new UTLSException("read too much");
            }
            if (l == size)
            {
                return buf;
            }
            byte[] r = new byte[l];
            Array.Copy(buf, 0, r, 0, l);
            return r;
        }

        static void EnableWeakCipher()
        {
            C.utls_enable_weak_cipher();
        }
        
        /*
                public static extern int utls_read_server_name(int h, [In, Out] byte[] pData, uint lSizeOfData);
        */
    }

    enum FingerprintID
    {
        Random,
        Qihoo360,
        AndroidOkHttp,
        Chrome,
        Edge,
        Firefox,
        Go,
        IOS,
        QQ,
    }
}
