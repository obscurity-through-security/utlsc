using System;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Nutls
{
    public class UTLSStream : AuthenticatedStream
    {
        Handle h;
        RemoteCertificateValidationCallback validationCallback;
        LocalCertificateSelectionCallback selectionCallback;

        public UTLSStream(
            Stream innerStream,
            bool leaveInnerStreamOpen = false,
            RemoteCertificateValidationCallback userCertificateValidationCallback = null,
            LocalCertificateSelectionCallback userCertificateSelectionCallback = null,
            EncryptionPolicy ep = EncryptionPolicy.RequireEncryption
        ) : base(innerStream, leaveInnerStreamOpen)
        {
            if (ep != EncryptionPolicy.RequireEncryption)
            {
                throw new NotSupportedException($"only {nameof(EncryptionPolicy.RequireEncryption)} is supported");
            }

            validationCallback = userCertificateValidationCallback;
            selectionCallback = userCertificateSelectionCallback;
        }

        #region stream state
        bool auth = false;
        public override bool IsAuthenticated => auth;
        public override bool IsEncrypted => IsAuthenticated;

        public override bool IsSigned => IsAuthenticated;
        public override bool CanRead => IsAuthenticated && InnerStream.CanRead;
        public override bool CanWrite => IsAuthenticated && InnerStream.CanWrite;
        public override bool CanTimeout => InnerStream.CanTimeout;

        public override bool IsMutuallyAuthenticated => false;
        public override bool IsServer => false;
        #endregion

        public override void Flush()
        {
            // todo
            InnerStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return h.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            h.Write(buffer, offset, count);
        }


        public async Task AuthenticateAsClientAsync(
            string targetHost,
            X509CertificateCollection clientCertificates = null,
            SslProtocols enabledSslProtocols = SslProtocols.None,
            bool checkCertificateRevocation = false
        )
        {
            h.SetServerName(targetHost);
            await Task.Factory.StartNew(() =>
            {
                h.Start();
            });
        }
        
        #region seek
        public override bool CanSeek => throw new System.NotSupportedException();
        public override long Length => throw new System.NotSupportedException();

        public override long Position { get => throw new System.NotSupportedException(); set => throw new System.NotSupportedException(); }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new System.NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new System.NotSupportedException();
        }
        #endregion
    }
}