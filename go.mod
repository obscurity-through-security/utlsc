module codeberg.org/obscurity-through-security/utlsc

go 1.20

require github.com/refraction-networking/utls v1.2.2

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/klauspost/compress v1.15.15 // indirect
	golang.org/x/crypto v0.6.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
)
