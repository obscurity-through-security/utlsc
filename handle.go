package utlsc

import (
	"crypto/x509"
	"errors"
	"net"
	"strings"

	tls "github.com/refraction-networking/utls"
)

type Handle struct {
	conf                *tls.Config
	skipServerNameCheck bool
	chs                 *tls.ClientHelloSpec
	e                   error

	conn  *tls.UConn
	lconn net.Conn
	rconn net.Conn
}

func NewHandle() *Handle {
	c1, c2 := net.Pipe()
	return &Handle{
		conf:  &tls.Config{},
		lconn: c1,
		rconn: c2,
	}
}

func (t *Handle) SetCA(ca *x509.CertPool) {
	t.conf.ClientCAs = ca
}

func (t *Handle) SetServerName(name string) {
	t.conf.ServerName = name
}

func (t *Handle) GetServerName() string {
	return t.conn.ConnectionState().ServerName
}

func (t *Handle) SkipCheck(off bool) {
	t.conf.InsecureSkipVerify = off
}

func (t *Handle) SetALPN(s string) {
	t.conf.NextProtos = strings.Split(s, ",")
}

func (t *Handle) SetALPS(app, config string) {
	if t.conf.ApplicationSettings == nil {
		t.conf.ApplicationSettings = map[string][]byte{}
	}
	t.conf.ApplicationSettings[app] = []byte(config)
}

func (t *Handle) GetALPS() []byte {
	return t.conn.ConnectionState().PeerApplicationSettings
}

const (
	FP_RANDOM = 0x0000 + iota
	FP_360
	FP_ANDROID_OKHTTP
	FP_CHROME
	FP_EDGE
	FP_FIREFOX
	FP_GO
	FP_IOS
	FP_QQ
)

func (t *Handle) SetFingerprintID(id int) error {
	var chid *tls.ClientHelloID
	switch id {
	default:
	case FP_RANDOM:
	case FP_360:
		chid = &tls.Hello360_Auto
	case FP_ANDROID_OKHTTP:
		chid = &tls.HelloAndroid_11_OkHttp
	case FP_CHROME:
		chid = &tls.HelloChrome_Auto
	case FP_EDGE:
		chid = &tls.HelloEdge_Auto
	case FP_FIREFOX:
		chid = &tls.HelloFirefox_Auto
	case FP_GO:
		chid = &tls.HelloGolang
	case FP_IOS:
		chid = &tls.HelloIOS_Auto
	case FP_QQ:
		chid = &tls.HelloQQ_Auto
	}
	spec, err := tls.UTLSIdToSpec(*chid)
	if err != nil {
		return err
	}
	t.chs = &spec
	return nil
}

var errOpened = errors.New("already opened")

func (t *Handle) Start() error {
	if t.conn != nil {
		return errOpened
	}

	conf := t.conf.Clone()
	if t.skipServerNameCheck {
		conf.InsecureServerNameToVerify = "*"
	}
	ch := tls.HelloRandomized
	if t.chs != nil {
		ch = tls.HelloCustom
	}
	uc := tls.UClient(t.lconn, conf, ch)
	if t.chs != nil {
		if err := uc.ApplyPreset(t.chs); err != nil {
			t.e = err
			return err
		}
	}
	t.conn = uc
	return nil
}

func (t *Handle) Write(b []byte) (int, error) {
	i, e := t.conn.Write(b)
	t.updateErr(e)
	return i, e
}

func (t *Handle) Read(b []byte) (int, error) {
	i, e := t.conn.Read(b)
	t.updateErr(e)
	return i, e
}

func (t *Handle) WriteTLS(b []byte) (int, error) {
	i, e := t.rconn.Write(b)
	t.updateErr(e)
	return i, e
}

func (t *Handle) ReadTLS(b []byte) (int, error) {
	i, e := t.rconn.Read(b)
	t.updateErr(e)
	return i, e
}

func (t *Handle) Close() error {
	err := t.conn.Close()
	t.rconn.Close()
	t.lconn.Close()
	return err
}

func (t *Handle) LastError() error {
	return t.e
}

func (t *Handle) updateErr(e error) {
	if e == nil {
		return
	}
	t.e = e
}

func (t *Handle) CloneFingerprint(b []byte, shuffle bool, sloppy bool) error {
	fp := tls.Fingerprinter{}
	if sloppy {
		fp.AllowBluntMimicry = true
		fp.AlwaysAddPadding = true
		fp.KeepPSK = true
	}
	spec, err := fp.FingerprintClientHello(b)
	if err != nil {
		return err
	}
	t.chs = spec
	return nil
}
