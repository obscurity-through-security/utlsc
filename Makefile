GO_MODULE:=$(shell go list)
LIBNAME:=$(shell go list -f '{{.Name}}')
OUT:=bin
CC=gcc

LIBPFX=lib
LIBEXT=so
EXE=utlsget
ifeq ($(OS),Windows_NT)
  LIBPFX=
  LIBEXT=dll
  EXE=utlsget.exe
endif

# If the first argument is "run"...
ifeq (run,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif


lib:
	go build -o $(OUT)/$(LIBPFX)$(LIBNAME).$(LIBEXT) -buildmode=c-shared $(GO_MODULE)/binding 

example: lib
	$(CC) example/main.c -l$(LIBNAME) -I$(OUT) -L$(OUT) -o $(OUT)/$(EXE)

run: example
	LD_LIBRARY_PATH=$(OUT) $(OUT)/$(EXE) $(RUN_ARGS)