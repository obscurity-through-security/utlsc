// Note: I can't really figure out how these code works, I'm not a C programmer
// Mostly copied from https://stackoverflow.com/a/35681089

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/stat.h>

#include <pthread.h>

#include "libutlsc.h"

struct utls_state
{
    int utls;
    int net;
};

#define BUFFER_N 65536

void *tls_to_wire(void *args)
{
    struct utls_state state = *(struct utls_state *)args;
    int u = state.utls;
    int n = state.net;
    char *buf = malloc(BUFFER_N);
    while (true)
    {
        int r = utls_read_tls(u, buf, BUFFER_N);
        if (r <= 0)
        {
            break;
        }

        int w = write(n, buf, r);
        if (w <= 0)
        {
            break;
        }
    }
    utls_close(u);
    close(n);
    return NULL;
}

void *wire_to_tls(void *args)
{
    struct utls_state state = *(struct utls_state *)args;
    int u = state.utls;
    int n = state.net;
    char *buf = malloc(BUFFER_N);
    while (true)
    {
        int r = read(n, buf, BUFFER_N);
        if (r <= 0)
        {
            break;
        }

        int w = utls_write_tls(u, buf, r);
        if (w <= 0)
        {
            break;
        }
    }
    utls_close(u);
    close(n);
    return NULL;
}

int main(int argc, char **argv)
{
    char buffer[BUFSIZ];
    char protoname[] = "tcp";
    struct protoent *protoent;
    char *server_hostname = "client.tlsfingerprint.io";
    char *user_input = NULL;
    in_addr_t in_addr;
    in_addr_t server_addr;
    int sockfd;
    size_t getline_buffer = 0;
    ssize_t nbytes_read, i, user_input_len;
    struct hostent *hostent;
    /* This is the struct used by INet addresses. */
    struct sockaddr_in sockaddr_in;
    unsigned short server_port = 8443;

    if (argc > 1)
    {
        server_hostname = argv[1];
        server_port = 443;
        if (argc > 2)
        {
            server_port = strtol(argv[2], NULL, 10);
        }
    }

    /* Get socket. */
    protoent = getprotobyname(protoname);
    if (protoent == NULL)
    {
        perror("getprotobyname");
        exit(EXIT_FAILURE);
    }
    sockfd = socket(AF_INET, SOCK_STREAM, protoent->p_proto);
    if (sockfd == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    /* Prepare sockaddr_in. */
    hostent = gethostbyname(server_hostname);
    if (hostent == NULL)
    {
        fprintf(stderr, "error: gethostbyname(\"%s\")\n", server_hostname);
        exit(EXIT_FAILURE);
    }
    in_addr = inet_addr(inet_ntoa(*(struct in_addr *)*(hostent->h_addr_list)));
    if (in_addr == (in_addr_t)-1)
    {
        fprintf(stderr, "error: inet_addr(\"%s\")\n", *(hostent->h_addr_list));
        exit(EXIT_FAILURE);
    }
    sockaddr_in.sin_addr.s_addr = in_addr;
    sockaddr_in.sin_family = AF_INET;
    sockaddr_in.sin_port = htons(server_port);

    /* utls setup */
    int ufd = utls_new();
    if (ufd < 0)
        fputs(utls_error(0), stderr);
    utls_set_server_name(ufd, server_hostname);
    utls_start(ufd);

    char *ch = malloc(8192);
    int chsize = 0;

    /* cache fingerprint is suggested */
    int fpfd = open("fingerprint.bin", O_RDONLY);
    if (fpfd < 0)
    {
        puts("generate fingerprint");
        chsize = utls_random_fingerprint(ch, 8192);
        fpfd = creat("fingerprint.bin", 0755);
        if (fpfd > 0)
        {
            write(fpfd, ch, chsize);
            close(fpfd);
        }
    }
    else
    {
        puts("load fingerprint");
        chsize = read(fpfd, ch, 8192);
    }
    utls_clone_fingerprint(ufd, ch, chsize, false, false);

    /* Do the actual connection. */
    if (connect(sockfd, (struct sockaddr *)&sockaddr_in, sizeof(sockaddr_in)) == -1)
    {
        perror("connect");
        return EXIT_FAILURE;
    }

    /* utls setup forwarder */
    struct utls_state state = {};
    state.utls = ufd;
    state.net = sockfd;
    pthread_t threads[2];
    pthread_create(threads, NULL, tls_to_wire, (void *)&state);
    pthread_create(threads + 1, NULL, wire_to_tls, (void *)&state);

    /* utls build http header */
    char *http = malloc(BUFFER_N);
    sprintf(http, "GET / HTTP/1.0\r\nHost: %s\r\nConnection: close\r\n\r\n", server_hostname);
    utls_write(ufd, http, strlen(http));

    char *buf = malloc(BUFFER_N);
    /* utls read http response */
    utls_read(ufd, buf, BUFFER_N);
    puts(buf);

    utls_close(ufd);
    exit(EXIT_SUCCESS);
}
