package main

/*
#include <string.h>
#include <stdbool.h>
*/
import "C"

import (
	"crypto/x509"
	"errors"
	"net"
	"unsafe"

	"codeberg.org/obscurity-through-security/utlsc"
	tls "github.com/refraction-networking/utls"
)

const (
	OK    = 0
	ERROR = -1
)

var lastErr error = nil
var ErrorNotExist = errors.New("utlsc: id not exist")
var ErrorOpened = errors.New("utlsc: already opened")

// passing index to avoid pass raw pointer and it's problem (mostly about crossing GC boundary)
// C binding is not only for C, but for every language
var (
	tunmap *utlsc.RWMap[C.int, *utlsc.Handle] = utlsc.NewRWMap[C.int, *utlsc.Handle]()
	idx    C.int                              = 1
)

func find(h C.int) *utlsc.Handle {
	t, ok := tunmap.Get(h)
	if !ok {
		lastErr = ErrorNotExist
		return nil
	}
	return t
}

// create a new utls handler
//
// note: function parameter name is using Windows API name convention
//
// pAbc: pointer to abc
//
// lAbc: long (int) variable abc
//
// bAbc: bool variable abc
//
// szAbc: zero terminated string (C string) abc
//
//export utls_new
func utls_new() C.int {
	t := utlsc.NewHandle()
	tunmap.Put(idx, t)
	r := idx
	idx++
	return r
}

// return latest error of handle, if handle is 0, return latest global error
//
//export utls_error
func utls_error(h C.int) *C.char {
	if h > 0 {
		t, ok := tunmap.Get(h)
		if !ok {
			return C.CString("not exist")
		}
		return C.CString(t.LastError().Error())
	}
	if lastErr == nil {
		return nil
	}
	return C.CString(lastErr.Error())
}

//export utls_set_ca
func utls_set_ca(h C.int, pCert *C.char, lSizeOfCert C.size_t) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	s := C.GoStringN(pCert, C.int(lSizeOfCert))
	c, err := x509.ParseCertificates([]byte(s))
	if err != nil {
		lastErr = err
		return ERROR
	}
	pool := x509.NewCertPool()
	for _, v := range c {
		pool.AddCert(v)
	}
	t.SetCA(pool)
	return OK
}

//export utls_set_server_name
func utls_set_server_name(h C.int, szName *C.char) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	s := C.GoString(szName)
	t.SetServerName(s)
	return OK
}

//export utls_skip_check
func utls_skip_check(h C.int, bCheck C.int) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	b := bCheck != C.false
	t.SkipCheck(b)
	return OK
}

//export utls_set_alpn
func utls_set_alpn(h C.int, szAlpn *C.char) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	s := C.GoString(szAlpn)
	t.SetALPN(s)
	return OK
}

//export utls_set_alps
func utls_set_alps(h C.int, szApp *C.char, pConfig *C.char, lSizeOfConfig C.size_t) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	app := C.GoString(szApp)
	config := C.GoStringN(pConfig, C.int(lSizeOfConfig))
	t.SetALPS(app, config)
	return OK
}

//export utls_read_alps
func utls_read_alps(h C.int, pData *C.char, lSizeOfData C.size_t) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	alps := t.GetALPS()
	lAlps := C.size_t(len(alps))
	if lAlps > lSizeOfData {
		return C.int(-lAlps)
	}
	C.memcpy(unsafe.Pointer(pData), C.CBytes(alps), lAlps)
	return C.int(lAlps)
}

//export utls_read_server_name
func utls_read_server_name(h C.int, pData *C.char, lSizeOfData C.size_t) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	name := t.GetServerName()
	lName := C.size_t(len(name))
	if lName+1 > lSizeOfData {
		return C.int(-lName)
	}
	C.memcpy(unsafe.Pointer(pData), C.CBytes([]byte(name)), lName)
	return C.int(lName)
}

const (
	FP_RANDOM = 0x0000 + iota
	FP_360
	FP_ANDROID_OKHTTP
	FP_CHROME
	FP_EDGE
	FP_FIREFOX
	FP_GO
	FP_IOS
	FP_QQ
)

//export utls_set_fingerprint
func utls_set_fingerprint(h C.int, lFingerprintID C.int) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	if err := t.SetFingerprintID(int(lFingerprintID)); err != nil {
		lastErr = err
		return ERROR
	}
	return OK
}

//export utls_start
func utls_start(h C.int) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	err := t.Start()
	if err != nil {
		lastErr = err
		return ERROR
	}
	return OK
}

//export utls_write
func utls_write(h C.int, pData *C.char, lSizeOfData C.size_t) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	s := C.GoBytes(unsafe.Pointer(pData), C.int(lSizeOfData))
	n, err := t.Write(s)
	if err != nil {
		lastErr = err
		return ERROR
	}
	return C.int(n)
}

//export utls_read
func utls_read(h C.int, pData *C.char, lSizeOfData C.size_t) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	buf := make([]byte, lSizeOfData)
	n, err := t.Read(buf)
	if err != nil {
		lastErr = err
		return ERROR
	}
	C.memcpy(unsafe.Pointer(pData), C.CBytes(buf), lSizeOfData)
	return C.int(n)
}

// read a TLS packet from uTLS handle
//
//export utls_read_tls
func utls_read_tls(h C.int, pData *C.char, lSizeOfData C.size_t) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	buf := make([]byte, lSizeOfData)
	n, err := t.ReadTLS(buf)
	if err != nil {
		lastErr = err
		return ERROR
	}
	C.memcpy(unsafe.Pointer(pData), C.CBytes(buf), lSizeOfData)
	return C.int(n)
}

// write a TLS packet into uTLS handle
//
//export utls_write_tls
func utls_write_tls(h C.int, pData *C.char, lSizeOfData C.size_t) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	s := C.GoBytes(unsafe.Pointer(pData), C.int(lSizeOfData))
	n, err := t.WriteTLS(s)
	if err != nil {
		lastErr = err
		return ERROR
	}
	return C.int(n)
}

//export utls_close
func utls_close(h C.int) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	tunmap.Remove(h)
	err := t.Close()
	if err != nil {
		lastErr = err
		return ERROR
	}
	return OK
}

//export utls_clone_fingerprint
func utls_clone_fingerprint(h C.int, pData *C.char, lSizeOfData C.size_t, bShuffle C.int, bSloppy C.int) C.int {
	t := find(h)
	if t == nil {
		return ERROR
	}
	b := C.GoBytes(unsafe.Pointer(pData), C.int(lSizeOfData))
	shuffle := bShuffle != C.false
	sloppy := bSloppy != C.false
	t.CloneFingerprint(b, shuffle, sloppy)

	return OK
}

//export utls_random_fingerprint
func utls_random_fingerprint(pData *C.char, lSizeOfData C.size_t) C.int {
	conn, _ := net.Pipe()

	uc := tls.UClient(conn, &tls.Config{InsecureSkipVerify: true}, tls.HelloRandomized)
	if err := uc.BuildHandshakeState(); err != nil {
		lastErr = err
		return ERROR
	}
	ch := uc.HandshakeState.Hello
	chb := ch.Marshal()
	conn.Close()
	C.memcpy(unsafe.Pointer(pData), C.CBytes(chb), C.size_t(len(chb)))
	return C.int(len(chb))
}

// todo utls should export u_parrots.go:shuffleExtensions

//export utls_enable_weak_cipher
func utls_enable_weak_cipher() {
	tls.EnableWeakCiphers()
}

func main() {}
